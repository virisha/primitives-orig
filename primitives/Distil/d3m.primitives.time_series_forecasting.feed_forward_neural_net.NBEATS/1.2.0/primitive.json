{
    "id": "3952a074-145e-406d-9cee-80232ae8f3ae",
    "version": "1.2.0",
    "name": "NBEATS",
    "keywords": [
        "time series",
        "forecasting",
        "deep neural network",
        "fully-connected",
        "residual network",
        "interpretable"
    ],
    "source": {
        "name": "Distil",
        "contact": "mailto:jeffrey.gleason@kungfu.ai",
        "uris": [
            "https://github.com/kungfuai/d3m-primitives"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package": "cython",
            "version": "0.29.16"
        },
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/kungfuai/d3m-primitives.git@66a2eac2d8f31fe3d892ffc5f1b7a56c4fbfd6af#egg=kf-d3m-primitives"
        }
    ],
    "python_path": "d3m.primitives.time_series_forecasting.feed_forward_neural_net.NBEATS",
    "algorithm_types": [
        "DEEP_NEURAL_NETWORK"
    ],
    "primitive_family": "TIME_SERIES_FORECASTING",
    "can_use_gpus": true,
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "kf_d3m_primitives.ts_forecasting.nbeats.nbeats.NBEATSPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "kf_d3m_primitives.ts_forecasting.nbeats.nbeats.Params",
            "Hyperparams": "kf_d3m_primitives.ts_forecasting.nbeats.nbeats.Hyperparams"
        },
        "interfaces_version": "2020.11.3",
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "weights_dir": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": "nbeats_weights",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "weights of trained model will be saved to this filepath"
            },
            "prediction_length": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 30,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "number of future timesteps to predict",
                "lower": 1,
                "upper": 1000,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "interpretable": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to build interpretable architecture"
            },
            "num_context_lengths": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 2,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "number of different context lengths to use for training estimators",
                "lower": 1,
                "upper": 6,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "num_estimators": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 2,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "number of different estimators to train for each combination of context length and loss function (3). The total number of estimators is num_estimators * num_context_lengths * 3",
                "lower": 1,
                "upper": 20,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "epochs": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 10,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "number of training epochs for each estimator",
                "lower": 1,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "steps_per_epoch": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 50,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "number of steps per epoch for each estimator",
                "lower": 1,
                "upper": 200,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "learning_rate": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.0001,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "learning rate for each estimator",
                "lower": 0.0,
                "upper": 1.0,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "training_batch_size": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 32,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "training batch size for each estimator",
                "lower": 1,
                "upper": 256,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "inference_batch_size": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 256,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "inference batch size",
                "lower": 1,
                "upper": 1024,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "output_mean": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to output mean (or median) forecasts from ensemble estimators. If `interpretable` is `True`, `output_mean` will automatically be `True` to preserve the additive decomposition of the trend and seasonality forecast components"
            },
            "nan_padding": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to pad predictions that aren't supported by the model with 'np.nan' or with the last valid prediction"
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "kf_d3m_primitives.ts_forecasting.nbeats.nbeats.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "kf_d3m_primitives.ts_forecasting.nbeats.nbeats.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Fits NBEATS model using training data from set_training_data and hyperparameters\n\nKeyword Arguments:\n    timeout {float} -- timeout, considered (default: {None})\n    iterations {int} -- iterations, considered (default: {None})\n\nReturns:\n    CallResult[None]\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods`` and ``set_training_data``.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "kf_d3m_primitives.ts_forecasting.nbeats.nbeats.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's predictions for specific time series at specific future time instances\n* these specific timesteps / series are specified implicitly by input dataset\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing attributes\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: if primitive not fit\n\nReturns:\n    CallResult[Outputs] -- (N, 2) dataframe with d3m_index and value for each prediction slice requested.\n        prediction slice = specific horizon idx for specific series in specific regression\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets primitive's training data\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing attributes\n    outputs {Outputs} -- D3M dataframe containing targets\n\nRaises:\n    ValueError: If multiple columns are annotated with 'Time' or 'DateTime' metadata\n\nParameters\n----------\ninputs:\n    The inputs.\noutputs:\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "nbeats_dataset": "kf_d3m_primitives.ts_forecasting.nbeats.nbeats_dataset.NBEATSDataset",
            "is_fit": "bool",
            "timestamp_column": "int",
            "freq": "str",
            "reind_freq": "str",
            "group_cols": "typing.List[int]",
            "output_column": "str",
            "target_column": "int",
            "min_trains": "typing.Union[typing.Dict[str, pandas._libs.tslibs.timestamps.Timestamp], typing.Dict[typing.Any, pandas._libs.tslibs.timestamps.Timestamp], typing.List[pandas._libs.tslibs.timestamps.Timestamp]]"
        }
    },
    "structural_type": "kf_d3m_primitives.ts_forecasting.nbeats.nbeats.NBEATSPrimitive",
    "description": "This primitive applies the Neural basis expansion analysis for interpretable time\nseries forecasting (NBEATS) method for time series forecasting. The implementation is based off of\nthis paper: https://arxiv.org/abs/1905.10437 and this repository: https://gluon-ts.mxnet.io/index.html\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "37bad177533cd5421a57a7cd695884b94d83c5775b928af3ef8deb6687129d1e"
}
